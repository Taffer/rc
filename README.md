# rc

Dotfiles, just the way I like 'em.

These are under the [MIT license](LICENSE.md), I hope they're useful!

I've been considering moving to [chezmoi](https://www.chezmoi.io/), but...
inertia...
