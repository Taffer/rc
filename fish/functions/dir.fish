# Like DOS, but useful.
function dir
    if command -qs {$EXA_COMMAND}
        {$EXA_COMMAND} --icons --color=auto --long $argv
    else
        if test (uname) = 'Darwin'
            # Mac uses ancient BSD variants.
            ls -BFGhl $argv
        else
            ls --color=auto -BFGhl $argv
        end
    end
end
